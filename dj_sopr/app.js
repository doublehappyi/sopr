var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swig = require('swig');
var session = require('express-session');
var passport = require('passport');
var auth = require('./middlewares/auth');
var localStrategy = auth.localStrategy;

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('view cache', false);
swig.setDefaults({cache: false});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(require('express-session')({secret:'secret', resave:true, saveUninitialized:true, cookie:{expires:1000*60}}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(localStrategy);

//app.use(helmet);
//app.use(session({
//    name:'soprsid',
//    secret:'secretkey123456789',
//    resave: true,
//    saveUninitialized: true,
//    cookie:{
//        httpOnly:true
//    }
//}));

//app.use(auth.checkLogin);
//app.use(auth.checkPermission);

app.use('/', require('./controllers/index'));
app.use('/', require('./controllers/user'));
app.use('/', require('./controllers/group'));
app.use('/', require('./controllers/modules'));
app.use('/', require('./controllers/hotword'));



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
