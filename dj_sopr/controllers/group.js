/**
 * Created by db on 16/1/4.
 */
var router = require('express').Router();
//var logger = require('../lib/logger')("group");
var group = require('../models/group');
var validate = require('../middlewares/validate');

router.get('/group', function (req, res, next) {
    res.render('group/index.html');
});

router.get('/group/groupList', function (req, res, next) {
    var name = req.query.name||"",
        state = parseInt(req.query.state);
    state = (state === 1 || state === 0) ? state : "";
    group.queryList(name, state, function (err, rows, fields) {
        if (err) res.json({code: 0, msg: err});
        res.json({code: 1, msg: "ok", rows: rows});
    });
});

router.put('/group/groupEdit', function (req, res, next) {
    var id = parseInt(req.body.id);
    var name = req.body.name;
    var state = parseInt(req.body.state) === 0 ? 0 : 1;
    var modulesList = req.body.modulesList;
    group.groupEdit(id, name, state, modulesList, function (err, rows, fields) {
        if (err) {
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

router.put('/group/groupState', function (req, res, next) {
    var id = parseInt(req.body.id);
    var state = parseInt(req.body.state) === 0 ? 0 : 1;
    group.groupState(id, state, function (err, rows, fields) {
        if (err) {
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});
//
router.post('/group/groupAdd', function (req, res, next) {
    var name = req.body.name;
    var state = parseInt(req.body.state) === 0 ? 0 : 1;
    var modulesList = req.body.modulesList;
    console.log(name, state, modulesList);
    group.groupAdd(name, state, modulesList, function (err, rows, fields) {
        if (err) {
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});


module.exports = router;