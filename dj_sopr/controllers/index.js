/**
 * Created by db on 16/1/4.
 */
var router = require('express').Router();
var logger = require('../lib/logger')("user");
var auth = require('../middlewares/auth');
router.get('/',auth.isAuthenticated, function(req, res, next){
    res.render('index.html', {username:req.session.username, group_name:req.session.group_name});
});

router.get('/500', function(req, res, next){
    res.render('500.html');
});

router.get('/403', function(req, res, next){
    res.render('403.html');
});

module.exports = router;