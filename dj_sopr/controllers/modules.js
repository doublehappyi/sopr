/**
 * Created by db on 16/1/4.
 */
var router = require('express').Router();
var logger = require('../lib/logger')("modules");
var modules = require('../models/modules');

router.get('/modules', function(req, res, next){
    res.render('modules/index.html');
});

router.get('/modules/modulesList', function (req, res, next) {
    var name = req.query.name||"",
        title = req.query.title||"",
        state = parseInt(req.query.state);
    modules.queryList(name, title, state, function(err, rows, fields){
        if(err) res.json({code:0, msg:err});
        res.json({code:1, msg:"ok", rows:rows});
    })
});

router.post('/modules/modulesAdd', function (req, res, next) {
    var name = req.body.name,
        title = req.body.title,
        state = parseInt(req.body.state),
        file = req.body.file,
        script = req.body.script,
        url = req.body.url;
    logger.debug(name, title, state, file, script, url);
    modules.modulesAdd(name, title, state, file, script, url, function(err, rows, fields){
        if(err) res.json({code:0, msg:err});
        res.json({code:1, msg:"ok", rows:rows});
    });
});

router.put('/modules/modulesEdit', function (req, res, next) {
    var id = parseInt(req.body.id),
        title = req.body.title,
        state = parseInt(req.body.state),
        file = req.body.file,
        script = req.body.script,
        url = req.body.url;
    modules.modulesEdit(id, title, state, file, script, url, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

router.put('/modules/modulesState', function (req, res, next) {
    var id = parseInt(req.body.id);
    var state = parseInt(req.body.state);
    modules.modulesState(id, state, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

module.exports = router;