/**
 * Created by db on 16/1/4.
 */
var router = require('express').Router();
var logger = require('../lib/logger')("user");
var user = require('../models/user');
var passport = require('passport');
router.get('/user', function (req, res, next) {
    res.render('user/index.html');
});

router.get('/signin', function (req, res, next) {
    res.render('user/signin.html');
});

router.post('/signin',
    passport.authenticate('local', {
        successRedirect: '/',
    failureRedirect: '/signin'
})
);

//router.post('/signin', function (req, res, next) {
//    var username = req.body.username,
//        password = req.body.password;
//    console.log('username', username, 'password: ', password);
//    user.query(username, password, function(err, rows, fields){
//        if(err){
//            res.json({code:0, msg:"用户名或密码错误"});
//        }else if(rows.length > 0){
//            req.session.username = username;
//            req.session.user_id = rows[0].id;
//            req.session.group_id = rows[0].group_id;
//            req.session.group_name = rows[0].group_name;
//
//            console.log(req.session);
//
//            res.json({code: 1, msg: 'OK', rows:rows});
//        }else{
//            res.json({code:0, msg:"用户名或密码错误"});
//        }
//    });
//});

router.get('/user/userList', function (req, res, next) {
    var filter = {
        username: req.query.username,
        group_id: req.query.group_id,
        state: req.query.state
    };
    var page = parseInt(req.query.page);
    user.queryList(page, filter, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK', rows: rows});
    });
});

router.get('/user/userDetail', function (req, res, next) {
    var id = parseInt(req.query.id);
    user.queryDetail(id, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK', rows: rows});
    });
});

router.put('/user/userPassword', function (req, res, next) {
    var id = parseInt(req.body.id);
    user.userPassword(id, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

router.put('/user/userEdit', function (req, res, next) {
    var id = parseInt(req.body.id);
    var group_id = parseInt(req.body.group_id);
    var state = parseInt(req.body.state);
    user.userEdit(id, group_id, state, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

router.put('/user/userState', function (req, res, next) {
    var id = parseInt(req.body.id);
    var state = parseInt(req.body.state);
    user.userState(id, state, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

router.post('/user/userAdd', function (req, res, next) {
    var username = req.body.username;
    var group_id = parseInt(req.body.group_id);
    var state = parseInt(req.body.state) === 0 ? 0:1;
    user.userAdd(username, group_id, state, function (err, rows, fields) {
        if(err){
            res.json({code: 0, msg: err});
        }
        res.json({code: 1, msg: 'OK'});
    });
});

module.exports = router;