CREATE DATABASE IF NOT EXISTS sopr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use sopr;

#用户表
CREATE TABLE IF NOT EXISTS sopr_user(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(16) UNIQUE NOT NULL,
  password VARCHAR(16) NOT NULL,
  group_id INT(10) NOT NULL,
  state INT(1) DEFAULT 0,
  last_login_time INT(10) DEFAULT 0,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_user VALUES (1, 'admin', 'admin', 1, 1, 1451635482, 1451635482);

#角色表
CREATE TABLE IF NOT EXISTS sopr_group(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(16) UNIQUE NOT NULL,
  state INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_group VALUES (1, '超级管理员', 0, 1451635482);

#功能模块表
CREATE TABLE IF NOT EXISTS sopr_modules(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(16) UNIQUE NOT NULL,
  title VARCHAR(128) UNIQUE NOT NULL,
  url VARCHAR(255) UNIQUE NOT NULL,
  file VARCHAR(64) DEFAULT NULL,
  script VARCHAR(64) DEFAULT NULL,
  state INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);
insert into sopr_modules values(1, 'user', '用户管理', 'url1', 'file1', 'script1', 0, 1451635486);
insert into sopr_modules values(2, 'group', '角色管理', 'url2', 'file2', 'script1', 0, 1451635486);
insert into sopr_modules values(3, 'modules', '模块管理', 'url3', 'file3', 'script1', 0, 1451635486);

#角色模块关联表
CREATE TABLE IF NOT EXISTS sopr_group_modules(
  group_id INT(10) NOT NULL,
  modules_id INT(10) NOT NULL,
  PRIMARY KEY (group_id, modules_id)
);
insert into sopr_group_modules values(1, 1);
insert into sopr_group_modules values(1, 2);
insert into sopr_group_modules values(1, 3);




CREATE TABLE `hotwords` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`keywords` varchar(16) NOT NULL COMMENT '热词',
`sort_num` int(11) NOT NULL DEFAULT '999' COMMENT '排序，asc，保留字段',
`intervention` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1,人工干预；0，系统生成',
`conversion` bigint(20) NOT NULL DEFAULT '0' COMMENT '转化次数',
`max_result_count` bigint(20) NOT NULL DEFAULT '0' COMMENT '最大命中数',
`max_result_return` bigint(20) NOT NULL DEFAULT '0' COMMENT '最大反馈数',
`query_count` bigint(20) NOT NULL DEFAULT '0' COMMENT '查询次数',
`org_code` bigint(20) NOT NULL COMMENT '商家ID',
`store_id` bigint(20) NOT NULL COMMENT '门店ID',
`create_time` datetime NOT NULL,
`type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '词性（0，泛热词；1，商品名称；2，品牌；3，广告；4，商品前缀关键字；5，商品后缀关键字；6，店铺名称；7，分类名称；8，商家名称）',
 PRIMARY KEY (`id`),
 KEY `hotwords_storeId` (`store_id`) USING BTREE ) ENGINE=InnoDB AUTO_INCREMENT=1873084 DEFAULT CHARSET=utf8 COMMENT='热词信息表'
