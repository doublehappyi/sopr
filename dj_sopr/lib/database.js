/**
 * Created by db on 16/1/5.
 */

var mysql = require('mysql');
var transaction = require('node-mysql-transaction');
var db_conf = {
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '11111111',
    database: 'sopr'
}
var pool = mysql.createPool(db_conf);

var table = {
    group: "sopr_group",
    user: 'sopr_user',
    modules: 'sopr_modules',
    log: 'sopr_log',
    group_modules: 'sopr_group_modules'
}

module.exports = {
    pool:pool,
    table: table,
    query: function (sql_str, callback) {
        console.log('sql_str: ', sql_str);
        pool.getConnection(function (err, conn) {
            if (err) {
                callback(err);
                return;
            }
            conn.query(sql_str, function (err, rows, fields) {
                if (err) {
                    callback(err);
                    return;
                }
                callback(err, rows, fields);
                conn.release();
            });
        });
    },
    createTransaction: function () {
        return transaction({
            // mysql driver set
            connection: [mysql.createConnection, db_conf],
            // create temporary connection for increased volume of async work.
            // if request queue became empty,
            // start soft removing process of the connection.
            // recommended for normal usage.
            dynamicConnection: 32,

            // set dynamicConnection soft removing time.
            idleConnectionCutoffTime: 1000,

            // auto timeout rollback time in ms
            // turn off is 0
            timeout: 600
        }).chain();
    }
}

