/**
 * Created by db on 16/1/5.
 */
var log4js = require('log4js');
log4js.configure({
    appenders: [
        {type: 'console'},
        {
            type: 'file',
            filename: 'log/sopr.log',
            maxLogSize: 1024,
            backups: 4,
            category: 'normal'
        }
    ],
    replaceConsole: true
});

module.exports = function logger(name){
    var logger = log4js.getLogger(name);
    logger.setLevel('DEBUG');
    return logger;
}