/**
 * Created by db on 16/1/5.
 */
var modules = require('../models/modules');
var group = require('../models/group');
var user = require('../models/user');
var db = require('../lib/database');
var swig = require('swig');
var mysql = require('mysql');
var logger = require('../lib/logger')(__filename);

var no_permission_pages = ['signin', '500', '403']
var no_login_pages = ['/signin', '/500', '/403'];
module.exports = {
    checkLogin:function(req, res, next){
        var signin = '/signin';
        var originalUrl = req.originalUrl;

        if(no_login_pages.indexOf(originalUrl) < 0 && !req.session.username){
            res.redirect(signin);
        }else{
            next();
        }
    },
    checkPermission:function(req, res, next){
        var group_id = req.session.group_id;
        //约定：当前根路径下第一个路径名，就是当前模块名！
        var modules_name = req.originalUrl.split('/')[1];
        //如果模块名为空，或者模块是无需验证的模块
        if(!modules_name || no_permission_pages.indexOf(modules_name) >=0){
            next();
        }else{
            var sql_str = 'select group_id from {{group_modules}} where group_id=? and modules_id=(select id from {{modules}} where name=?)';
            sql_str = swig.compile(sql_str)({group_modules:db.table.group_modules, modules:db.table.modules});
            sql_str = mysql.format(sql_str, [group_id, modules_name]);

            logger.debug(sql_str);
            db.query(sql_str, function(err, rows, fields){
                if(err) res.render('/500.html');
                if(rows && rows.length > 0){
                    next()
                }else{
                    res.render('/403.html');
                }
            });
        }
    }
}