/**
 * Created by yishuangxi on 2016/1/5.
 */
var mysql = require('mysql');
var swig = require('swig');
var db = require('../lib/database');

function queryAllList(callback){
    var sql_str = 'select id, name, state, create_time from {{group}}';
    sql_str = swig.compile(sql_str)({group: db.table.group});
    db.query(sql_str, callback);
}

function queryList(name, state, callback) {
    var sql_str = 'select id, name, state, create_time, ' +
        '(select group_concat(modules_id) from {{group_modules}} where group_id={{group}}.id) as modulesList ' +
        'from {{group}} where name like ? ';
    var name = ("%" + (name||"") + "%");
    if (state === 1 || state === 0) {
        sql_str += 'and state=?';
    }
    //模板替换表名
    sql_str = swig.compile(sql_str)({group: db.table.group, group_modules: db.table.group_modules});
    //sql语句生成
    sql_str = mysql.format(sql_str, [name, state]);
    db.query(sql_str, callback);
}

function queryDetail(id, callback) {
    var sql_str = 'select id, name, group_id, state, create_time, ' +
        '(select id from {{group}} where id={{group}}.group_id) as group from {{group}} where id=?';
    sql_str = swig.compile(sql_str)({group: db.table.group});
    sql_str = mysql.format(sql_str, value_arr);
    db.query(sql_str, callback);
}

function groupEdit(id, name, state, modulesList, callback) {
    //更新group表
    var sql_str_update = 'update {{group}} set name=?, state=? where id=?';
    sql_str_update = swig.compile(sql_str_update)({group: db.table.group});
    sql_str_update = mysql.format(sql_str_update, [name, state, id]);

    //构建删除sql
    var sql_str_del = 'delete from {{group_modules}} where group_id=?';
    sql_str_del = swig.compile(sql_str_del)({group_modules: db.table.group_modules});
    sql_str_del = mysql.format(sql_str_del, [id]);

    //构建插入sql
    var modules = modulesList.split(',');
    var values_insert = [];
    var values_insert_str = [];
    var sql_str_insert = 'insert into {{group_modules}} values ';
    for (var i = 0; i < modules.length; i++) {
        values_insert_str.push("(?, ?)");
        values_insert.push(id);
        values_insert.push(modules[i]);
    }
    sql_str_insert += values_insert_str.join(',');
    sql_str_insert = swig.compile(sql_str_insert)({group_modules: db.table.group_modules});
    sql_str_insert = mysql.format(sql_str_insert, values_insert);
    //执行事务：先删除旧关系数据，后插入新关系数据
    var transaction = db.createTransaction();

    transaction.on('commit', function () {
        callback(null);
    }).on('rollback', function (err) {
        callback(err);
    });

    transaction.query(sql_str_update).
        query(sql_str_del).on('result', function (result) {
            if (modules.length > 0) {
                transaction.query(sql_str_insert);
            }
        }).autoCommit(false);
}

function groupAdd(name, state, modulesList, callback) {
    var create_time = parseInt(new Date().getTime() / 1000);
    var modules = modulesList.split(',');
    var insert_str_group = 'insert into {{group}} values(null, ?, ?, ?)';
    insert_str_group = swig.compile(insert_str_group)({group: db.table.group});
    insert_str_group = mysql.format(insert_str_group, [name, state, create_time]);

    var transaction = db.createTransaction();
    transaction.on('commit', function () {
        callback(null);
    }).on('rollback', function (err) {
        callback(err);
    });

    transaction.query(insert_str_group).on('result', function (result) {
        var id = result.insertId;
        var modules = modulesList.split(',');
        var values_insert = [];
        var values_insert_str = [];
        var insert_str_group_modules = 'insert into {{group_modules}} values ';
        for (var i = 0; i < modules.length; i++) {
            values_insert_str.push("(?, ?)");
            values_insert.push(id);
            values_insert.push(modules[i]);
        }
        insert_str_group_modules+= values_insert_str.join(',');
        insert_str_group_modules = swig.compile(insert_str_group_modules)({group_modules: db.table.group_modules});
        insert_str_group_modules = mysql.format(insert_str_group_modules, values_insert);
        
        transaction.query(insert_str_group_modules);
    }).autoCommit(false);

    //var insert_str_group_modules = 'insert into {{group_modules}} values ';
    //var modules = modulesList.split(',');
    //var values_insert = [];
    //var values_insert_str = [];
    //var insert_str_group_modules = 'insert into {{group_modules}} values ';
    //for (var i = 0; i < modules.length; i++) {
    //    values_insert_str.push("(?, ?)");
    //    values_insert.push(id);
    //    values_insert.push(modules[i]);
    //}
    //insert_str_group_modules+= values_insert_str.join(',');
    //insert_str_group_modules = swig.compile(insert_str_group_modules)({group_modules: db.table.group_modules});
    //insert_str_group_modules = mysql.format(insert_str_group_modules, values_insert);
}


function groupState(id, state, callback) {
    var sql_str = 'update {{group}} set state=? where id=?';
    sql_str = swig.compile(sql_str)({group: db.table.group});
    sql_str = mysql.format(sql_str, [state, id]);
    db.query(sql_str, callback);
}

module.exports = {
    queryList: queryList,
    queryDetail: queryDetail,
    groupEdit: groupEdit,
    groupState: groupState,
    groupAdd: groupAdd
}