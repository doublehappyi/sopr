/**
 * Created by yishuangxi on 2016/1/5.
 */

var mysql = require('mysql');
var swig = require('swig');
var db = require('../lib/database');

function queryList(name, title, state, callback) {
    var name = "%" + (name || "") + "%", title = "%" + (title || "" ) + "%";
    var sql_str = 'select id, name, title, file, script, url, state, create_time ' +
        'from {{modules}} ' +
        'where name like ? and title like ? ';
    console.log('state: ', state);
    if(state == 0 || state == 1){
        sql_str += "and state=?";
    }
    //模板替换表名
    sql_str = swig.compile(sql_str)({modules: db.table.modules});
    //sql语句生成
    sql_str = mysql.format(sql_str, [name, title, state]);
    db.query(sql_str, callback);
}


function modulesAdd(name, title, state, file, script, url, callback) {
    var create_time = parseInt(new Date().getTime() / 1000);
    var sql_str = 'insert into {{modules}} values (null, ?, ?, ?, ?, ?, ?, ?)';
    sql_str = swig.compile(sql_str)({modules: db.table.modules});
    sql_str = mysql.format(sql_str, [name, title, url, file, script, state, create_time]);
    db.query(sql_str, callback);
}

function modulesEdit(id, title, state, file, script, url, callback) {
    var sql_str = 'update {{modules}} set title=?, state=?, file=?, script=?, url=? where id=?';
    sql_str = swig.compile(sql_str)({modules: db.table.modules});
    sql_str = mysql.format(sql_str, [title, state, file, script, url, id]);
    db.query(sql_str, callback);
}

function modulesState(id, state, callback) {
    var sql_str = 'update {{modules}} set state=? where id=?';
    sql_str = swig.compile(sql_str)({modules: db.table.modules});
    sql_str = mysql.format(sql_str, [state, id]);
    db.query(sql_str, callback);
}

module.exports = {
    queryList: queryList,
    modulesAdd: modulesAdd,
    modulesEdit: modulesEdit,
    modulesState: modulesState
}