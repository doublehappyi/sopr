/**
 * Created by yishuangxi on 2016/1/5.
 */

var mysql = require('mysql');
var swig = require('swig');
var db = require('../lib/database');


function query(username, password, callback){
    var sql_str = 'select id, username, group_id, ' +
        '(select name from {{group}} where id={{user}}.id ) as  group_name ' +
        'from {{user}} where username=? and password=?';
    //模板替换表名
    sql_str = swig.compile(sql_str)({user: db.table.user, group: db.table.group});
    //sql语句生成
    sql_str = mysql.format(sql_str, [username, password]);
    db.query(sql_str, callback);
}
var page_size = 20;
function queryList(page, condition, callback) {
    var sql_str, key_arr = [], value_arr = [];
    for (var f in condition) {
        if (condition.hasOwnProperty(f) && condition[f]) {
            if (f == 'username') {
                key_arr.push(f + " like ?");
                value_arr.push('%' + condition[f] + '%');
            } else {
                key_arr.push(f + '=? ');
                value_arr.push(condition[f]);
            }
        }
    }
    sql_str = 'select id, username, group_id, state, create_time, ' +
        '(select name from {{group}} where id={{user}}.group_id) as group_name from {{user}} ';
    //添加过滤条件
    if (key_arr.length > 0) {
        sql_str += 'where ' + key_arr.join(' and ');
    }
    //添加分页条件
    sql_str += 'limit ' + ((page - 1) * page_size) + ',' + page_size;
    //模板替换表名
    sql_str = swig.compile(sql_str)({user: db.table.user, group: db.table.group});
    //sql语句生成
    sql_str = mysql.format(sql_str, value_arr);
    db.query(sql_str, callback);
}

function queryDetail(id, callback) {
    var sql_str = 'select id, username, group_id, state, create_time, ' +
        '(select id from {{group}} where id={{user}}.group_id) as group from {{user}} where id=?';
    sql_str = swig.compile(sql_str)({user: db.table.user, group: db.table.group});
    sql_str = mysql.format(sql_str, value_arr);
    db.query(sql_str, callback);
}

function userEdit(id, group_id, state, callback) {
    var sql_str = 'update {{user}} set group_id=? ,state=? where id=?';
    sql_str = swig.compile(sql_str)({user: db.table.user});
    sql_str = mysql.format(sql_str, [group_id, state, id]);
    db.query(sql_str, callback);
}

function userAdd(username, group_id, state, callback){
    var password = "88888888";
    var create_time = parseInt(new Date().getTime()/1000);
    var sql_str = 'insert into {{user}} values(null, ?, ?, ?, ?, null, ?)';
    sql_str = swig.compile(sql_str)({user:db.table.user});
    sql_str = mysql.format(sql_str, [username, password, group_id, state, create_time]);
    db.query(sql_str, callback);
}

function userPassword(id, callback) {
    var sql_str = 'update {{user}} set password=? where id=?';
    sql_str = swig.compile(sql_str)({user: db.table.user});
    sql_str = mysql.format(sql_str, ['88888888', id]);
    db.query(sql_str, callback);
}

function userState(id, state, callback) {
    var sql_str = 'update {{user}} set state=? where id=?';
    sql_str = swig.compile(sql_str)({user: db.table.user});
    sql_str = mysql.format(sql_str, [state, id]);
    db.query(sql_str, callback);
}


function queryCount(condition, callback) {
    var sql_str, key_arr = [], value_arr = [];
    for (var f in condition) {
        if (condition.hasOwnProperty(f) && condition[f]) {
            key_arr.push(f + '=? ');
            value_arr.push(condition[f]);
        }
    }
    sql_str = 'select count(id) from {{user}} ';
    //添加过滤条件
    if (key_arr.length > 0) {
        sql_str += 'where ' + key_arr.join(' and ');
    }
    //添加分页条件
    sql_str += 'limit ' + ((page - 1) * page_size + 1) + ',' + page_size;
    //模板替换表名
    sql_str = swig.compile(sql_str)({user: db.table.user, group: db.table.group});
    //sql语句生成
    sql_str = mysql.format(sql_str, value_arr);
    db.query(sql_str, callback);
}

module.exports = {
    query:query,
    queryList: queryList,
    queryDetail: queryDetail,
    userPassword: userPassword,
    userEdit: userEdit,
    userState:userState,
    userAdd:userAdd
}